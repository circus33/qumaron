using UnityEngine;

namespace FirstTask.Code
{
    public class GameInstaller : MonoBehaviour
    {
        [SerializeField] private Camera mainCamera;
        [SerializeField] private PointerHandler pointerHandler;
        [SerializeField] private SpotManager spotManager;

        private void Awake()
        {
            pointerHandler.Init(mainCamera);
            spotManager.Init(pointerHandler);
        }
    }
}
