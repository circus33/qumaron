using System.Collections.Generic;
using UnityEngine;

namespace FirstTask.Code
{
    public class WaypointsController
    {
        private readonly WaypointPresenter waypointPrefab;
        private readonly List<Waypoint> waypoints = new List<Waypoint>();

        public Vector3? CurrentWaypoint => waypoints.Count > 0 ? waypoints[0].Position : (Vector3?) null;
        public Vector3? NextWaypoint => waypoints.Count > 1 ? waypoints[1].Position : (Vector3?) null;

        public WaypointsController(WaypointPresenter waypointPrefab, PointerHandler pointerHandler)
        {
            this.waypointPrefab = waypointPrefab;

            pointerHandler.PointerClickEvent += OnPointerClick;
        }

        public void CurrentWaypointReached()
        {
            Waypoint waypoint = waypoints[0];
            Object.Destroy(waypoint.Instance.gameObject);
            waypoints.RemoveAt(0);

            for (var i = 0; i < waypoints.Count; i++)
            {
                Waypoint wpoint = waypoints[i];
                wpoint.Instance.SetIndex(i + 1);
            }
        }

        private void OnPointerClick(Vector3 pos)
        {
            WaypointPresenter newInstance = Object.Instantiate(waypointPrefab, pos, Quaternion.identity);
            newInstance.SetIndex(waypoints.Count + 1);

            waypoints.Add(new Waypoint(pos, newInstance));
        }
        
        private struct Waypoint
        {
            private readonly Vector3 position;
            private readonly WaypointPresenter instance;

            public Vector3 Position => position;
            public WaypointPresenter Instance => instance;

            public Waypoint(Vector3 position, WaypointPresenter instance)
            {
                this.position = position;
                this.instance = instance;
            }
        }
    }
}
