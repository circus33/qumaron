using TMPro;
using UnityEngine;

namespace FirstTask.Code
{
    public class WaypointPresenter : MonoBehaviour
    {
        [SerializeField] private TextMeshPro hintText;
        [SerializeField] private SpriteRenderer spriteRenderer;
        
        public void SetIndex(int index)
        {
            hintText.text = index.ToString();
            hintText.renderer.sortingOrder = index;
            spriteRenderer.sortingOrder = index;
        }
    }
}
