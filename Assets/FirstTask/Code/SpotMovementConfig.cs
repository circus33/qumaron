using UnityEngine;

namespace FirstTask.Code
{
    [CreateAssetMenu(fileName = "SpotMovementConfig", menuName = "Configs/SpotMovementConfig")]
    public class SpotMovementConfig : ScriptableObject
    {
        [SerializeField] private float maxSpeed;
        [SerializeField] private float minSpeed;
        [SerializeField] private float accelerationSpeed;
        [SerializeField] private float brakeSpeed;
        [SerializeField] private float brakeDistance;
        [SerializeField] private float stopDistance;

        public float MAXSpeed => maxSpeed;
        public float MINSpeed => minSpeed;
        public float AccelerationSpeed => accelerationSpeed;
        public float BrakeSpeed => brakeSpeed;
        public float BrakeDistance => brakeDistance;
        public float StopDistance => stopDistance;
    }
}
