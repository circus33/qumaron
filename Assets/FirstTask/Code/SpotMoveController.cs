using UnityEngine;

namespace FirstTask.Code
{
    public class SpotMoveController
    {
        private readonly Transform spotTransform;
        private readonly SpotMovementConfig movementConfig;
        private readonly WaypointsController waypointsController;

        private float currentSpeed;

        public SpotMoveController(Transform spotTransform, SpotMovementConfig movementConfig, WaypointsController waypointsController)
        {
            this.spotTransform = spotTransform;
            this.movementConfig = movementConfig;
            this.waypointsController = waypointsController;

            currentSpeed = movementConfig.MINSpeed;
        }

        public void ProcessFixedUpdate()
        {
            SpeedControl();
            MoveControl();
        }
        
        private void SpeedControl()
        {
            if (waypointsController.CurrentWaypoint != null)
            {
                Vector3 waypoint = (Vector3) waypointsController.CurrentWaypoint;
                float distance = Vector3.Distance(waypoint, spotTransform.position);
                SpeedState targetState;

                if (distance <= movementConfig.BrakeDistance && waypointsController.NextWaypoint == null)
                {
                    targetState = currentSpeed > movementConfig.MINSpeed ? SpeedState.Brake : SpeedState.Hold;
                }
                else if (currentSpeed < movementConfig.MAXSpeed)
                {
                    targetState = SpeedState.Accelerate;
                }
                else
                {
                    targetState = SpeedState.Hold;
                }

                switch (targetState)
                {
                    case SpeedState.Brake:
                        currentSpeed -= movementConfig.BrakeSpeed * Time.deltaTime;
                        break;
                    case SpeedState.Accelerate:
                        currentSpeed += movementConfig.AccelerationSpeed * Time.deltaTime;
                        break;
                }
            }
            else if (currentSpeed > movementConfig.MINSpeed)
            {
                currentSpeed = movementConfig.MINSpeed;
            }
        }
        
        private void MoveControl()
        {
            if (waypointsController.CurrentWaypoint != null)
            {
                Vector3 waypoint = (Vector3) waypointsController.CurrentWaypoint;
                float distance = Vector3.Distance(waypoint, spotTransform.position);

                if (distance < movementConfig.StopDistance)
                {
                    waypointsController.CurrentWaypointReached();
                }
                else
                {
                    Vector3 direction = (waypoint - spotTransform.position).normalized;
                    spotTransform.position += direction * (currentSpeed * Time.deltaTime);
                }
            }
        }

        private enum SpeedState
        {
            Brake,
            Hold,
            Accelerate
        }
    }
}
