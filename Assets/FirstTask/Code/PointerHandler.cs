using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace FirstTask.Code
{
    public class PointerHandler : MonoBehaviour, IPointerClickHandler
    {
        public event Action<Vector3> PointerClickEvent;

        private Camera currentCamera;

        public void Init(Camera mainCamera)
        {
            currentCamera = mainCamera;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Vector3 worldPoint = currentCamera.ScreenToWorldPoint(eventData.position);
            worldPoint.z = 0;
            
            PointerClickEvent?.Invoke(worldPoint);
        }
    }
}
