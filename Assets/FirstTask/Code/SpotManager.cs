using System;
using UnityEngine;

namespace FirstTask.Code
{
    public class SpotManager : MonoBehaviour
    {
        [SerializeField] private GameObject spotPrefab;
        [SerializeField] private WaypointPresenter waypointPrefab;
        [SerializeField] private SpotMovementConfig spotMovementConfig;
        
        private WaypointsController waypointsController;
        private SpotMoveController spotMoveController;
        private GameObject spotInstance;
        
        public void Init(PointerHandler pointerHandler)
        {
            spotInstance = Instantiate(spotPrefab, Vector3.zero, Quaternion.identity);
            
            waypointsController = new WaypointsController(waypointPrefab, pointerHandler);
            spotMoveController = new SpotMoveController(spotInstance.transform, spotMovementConfig, waypointsController);
        }

        private void FixedUpdate()
        {
            spotMoveController.ProcessFixedUpdate();
        }
    }
}
